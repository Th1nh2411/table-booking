// [08202312RIN] Create home logic
// Sử dụng JavaScript để theo dõi kích thước cửa sổ và thay đổi class của div con
var contentElement = document.querySelector(".content");
const resize_ob = new ResizeObserver(function (entries) {
  var productCols = document.getElementsByClassName("col");
  // since we are observing only a single element, so we access the first element in entries array
  let rect = entries[0].contentRect;

  // current width & height
  let width = rect.width;
  if (width < 500) {
    for (var i = 0; i < productCols.length; i++) {
      productCols[i].classList = "col col-12";
    }
  } else if (width < 750) {
    for (var i = 0; i < productCols.length; i++) {
      productCols[i].classList = "col col-6";
    }
  } else if (width < 1000) {
    for (var i = 0; i < productCols.length; i++) {
      productCols[i].classList = "col col-4";
    }
  } else {
    for (var i = 0; i < productCols.length; i++) {
      productCols[i].classList = "col col-3";
    }
  }
});

resize_ob.observe(contentElement);
