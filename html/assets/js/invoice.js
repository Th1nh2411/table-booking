// [08202312RIN] Create invoice logic
const invoice = document.querySelector(".invoice");
const contentContainer = document.querySelector(".content");
const tableItems = document.querySelectorAll(".table");
const closeBtn = document.querySelector(".invoice-close");
const dateLabel = document.querySelector(".invoice-date");
const realTimeLabel = document.querySelector(".invoice-time");
const tableNumberLabel = document.querySelector(".invoice-table__number");
const tableStatusLabel = document.querySelector(".invoice-table__status");
var currentTime = new Date();
var formattedDate = currentTime.toLocaleString("en-US", {
  weekday: "short",
  month: "short",
  day: "numeric",
  year: "numeric",
});
var formattedTime = currentTime.toLocaleString("en-US", {
  hour: "numeric",
  minute: "numeric",
  hour12: true,
});
dateLabel.innerHTML = formattedDate;
realTimeLabel.innerHTML = formattedTime;

tableItems.forEach((item) => {
  item.onclick = function () {
    invoice.classList.add("open");
    if (!widthInvoiceMatch.matches) {
      contentContainer.style.paddingRight = "var(--invoice-width)";
    }
    const tableNumber = item.querySelector(".table-number").innerHTML;
    const tableStatus = item.querySelector(".table-status").innerHTML;
    var tableTime = item.querySelector(".table-time") || "";
    if (tableTime) {
      tableTime = item.querySelector(".table-time").innerHTML;
    }
    tableNumberLabel.innerHTML = "Table " + tableNumber;
    tableStatusLabel.innerHTML = tableStatus + " " + tableTime;
  };
});
closeBtn.onclick = function () {
  invoice.classList.remove("open");
  if (!widthInvoiceMatch.matches) {
    contentContainer.style.paddingRight = "0px";
  }
};

function handleInvoiceResponsive(widthInvoiceMatch) {
  if (widthInvoiceMatch.matches) {
    contentContainer.style.paddingRight = "0px";
  } else {
    invoice.classList.remove("open");
  }
}

var widthInvoiceMatch = window.matchMedia("(max-width: 900px)");
handleInvoiceResponsive(widthInvoiceMatch); // Call listener function at run time
widthInvoiceMatch.addListener(handleInvoiceResponsive); // Attach listener function on state changes
// handlePaymentSelect
function handleChangePaymentType(element) {
  var paymentTypeElements = document.querySelectorAll(
    ".invoice-payment-select"
  );

  // Lặp qua từng phần tử và loại bỏ class active
  paymentTypeElements.forEach(function (el) {
    el.classList.remove("active");
  });

  // Thêm class active cho phần tử được click
  element.classList.add("active");
}
